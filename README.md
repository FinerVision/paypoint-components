# PayPoint Components

This is an NPM package that houses all commonly used components between
all PayPoint sites.

### Installation

```bash
npm install --save paypoint-components
```

### Usage

Components are used by importing them into the PayPoint source.

#### Sass

Import the Sass component you want.

```sass
@import "~paypoint-components/src/sass/Header"
```

#### JavaScript

Import the JavaScript component you want.

```js
import Header from "paypoint-components/src/js/Header"
```
